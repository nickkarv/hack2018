import java.io.IOException;
import java.io.*;
import java.net.MalformedURLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "AceiteOlivetto",
    urlPatterns = {"/AceiteOlivetto"}
)
public class AceiteOlivetto extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
      
      int Mes = Integer.parseInt(request.getParameter("Mes"));
      String Sede = request.getParameter("Sede");      
     
      String  wml_service_credentials_url = "https://us-south.ml.cloud.ibm.com";
String wml_service_credentials_username = "fc8afda2-f092-4f9f-aeb2-7a9da0be7977";
      String wml_service_credentials_password = "4abcb306-1c95-40c3-ada1-9c30e54df3a6";
      Map<String, String> wml_credentials = new HashMap<String, String>()
	{{
		put("url", wml_service_credentials_url);
		put("username", wml_service_credentials_username);
		put("password", wml_service_credentials_password);
	}};

String wml_auth_header = "Basic " +
		Base64.getEncoder().encodeToString((wml_credentials.get("username") + ":" +
			wml_credentials.get("password")).getBytes(StandardCharsets.UTF_8));
      System.out.print(wml_credentials.get("username")+"\n" + wml_credentials.get("password")+"\n"+wml_credentials.get("url")+"\n" );
String wml_url = wml_credentials.get("url") + "/v3/identity/token";
HttpURLConnection tokenConnection = null;
HttpURLConnection scoringConnection = null;
BufferedReader tokenBuffer = null;
BufferedReader scoringBuffer = null;
try {
	// Getting WML token
	URL tokenUrl = new URL(wml_url);
              System.out.print(tokenUrl +"\n");
	tokenConnection = (HttpURLConnection) tokenUrl.openConnection();
              System.out.print(tokenConnection +"\n");
	tokenConnection.setDoInput(true);
	tokenConnection.setDoOutput(true);
	tokenConnection.setRequestMethod("GET");
	tokenConnection.setRequestProperty("Authorization", wml_auth_header);
              System.out.print("haed;;; " + wml_auth_header +"\n");
	tokenBuffer = new BufferedReader(new InputStreamReader(tokenConnection.getInputStream()));
	StringBuffer jsonString = new StringBuffer();
	String line;
	while ((line = tokenBuffer.readLine()) != null) {
		jsonString.append(line);
	}
              
	// Scoring request
	URL scoringUrl = new URL("	https://us-south.ml.cloud.ibm.com/v3/wml_instances/3c03c53a-dc91-4c55-ae5a-506740304f3a/deployments/f04a0bee-f7be-4055-95ec-3f3814a37fbe/online");
              System.out.print(scoringUrl +"\n");
	String wml_token = "Bearer " +
			jsonString.toString()
					.replace("\"","")
					.replace("}", "")
					.split(":")[1];
              System.out.print(wml_token +"\n");
	scoringConnection = (HttpURLConnection) scoringUrl.openConnection();
              System.out.print(scoringUrl +"\n");
	scoringConnection.setDoInput(true);
	scoringConnection.setDoOutput(true);
	scoringConnection.setRequestMethod("POST");
	scoringConnection.setRequestProperty("Accept", "application/json");
	scoringConnection.setRequestProperty("Authorization", wml_token);
	scoringConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	OutputStreamWriter writer = new OutputStreamWriter(scoringConnection.getOutputStream(), "UTF-8");

	// NOTE: manually define and pass the array(s) of values to be scored in the next line
	String payload = "{\"fields\": [\"Mes\", \"Sedes\"], \"values\": [["+Mes+", \""+Sede+"\"]]}";
	System.out.print(payload +"\n");
              writer.write(payload);                        
	writer.close();

	scoringBuffer = new BufferedReader(new InputStreamReader(scoringConnection.getInputStream()));
	StringBuffer jsonStringScoring = new StringBuffer();
	String lineScoring;
	while ((lineScoring = scoringBuffer.readLine()) != null) {
		jsonStringScoring.append(lineScoring);
	}
	System.out.println(jsonStringScoring);
              int cuentacomas = 0;
              String res = "";
              String res2 = "";
              boolean terminerecorrerprimero = false;
              for (int i = jsonStringScoring.length() -1; i > 0; i--) {
                  if (jsonStringScoring.charAt(i) == ',') {
                      cuentacomas++;
                  }
                  if (cuentacomas ==  4) {
                      for (int j = i - 1;;j--) {
                        if (jsonStringScoring.charAt(j-1) != ',' && !terminerecorrerprimero) {
                          res += jsonStringScoring.charAt(j-1);
                        }
                        else {
                            terminerecorrerprimero = true;
                            if (jsonStringScoring.charAt(j-2) == '[') {
                                break;
                            }
                            res2+= jsonStringScoring.charAt(j-2);
                        }
                      }
                      break;
                  }
              }
              String sCadena = res;
              String sCadenaInvertida="";
              for (int x=sCadena.length()-1;x>=0;x--){
                  sCadenaInvertida = sCadenaInvertida + sCadena.charAt(x);
              }
              res=sCadenaInvertida;
              sCadena = res2;
              sCadenaInvertida = "";
              for (int x=sCadena.length()-1;x>=0;x--){
                  sCadenaInvertida = sCadenaInvertida + sCadena.charAt(x);
              }
              res2=sCadenaInvertida;
              res = res.replaceAll(" ", "");
              float ProbExito=Float.parseFloat(res);
              float ProbFallo=Float.parseFloat(res2);
              System.out.println(res);
              System.out.println(res2);
              
              response.getWriter().print("<html><head></script><style type=\"text/css\">/* Chart.js */\n" + 
              		"\n" + 
              		"@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style>\n" + 
              		"<script src= \"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js\"> </script>\n" + 
              		"\n" + 
              		"\n" + 
              		"<style>\n" + 
              		"\n" + 
              		"ul {\n" + 
              		"    list-style-type: none;\n" + 
              		"    margin: 0;\n" + 
              		"    padding: 0;\n" + 
              		"    overflow: hidden;\n" + 
              		"    background-color: #0fc43f;\n" + 
              		"    position: fixed;\n" + 
              		"    top: 0;\n" + 
              		"    width: 100%;\n" + 
              		"}\n" + 
              		"\n" + 
              		"li {\n" + 
              		"    float: left;\n" + 
              		"}\n" + 
              		"\n" + 
              		"li a {\n" + 
              		"    display: block;\n" + 
              		"    color: white;\n" + 
              		"    text-align: center;\n" + 
              		"    padding: 14px 16px;\n" + 
              		"    text-decoration: none;\n" + 
              		"}\n" + 
              		"\n" + 
              		"li a:hover:not(.active) {\n" + 
              		"    background-color: #6df272;\n" + 
              		"}\n" + 
              		"h1{\n" + 
              		"	text-align: center;\n" + 
              		"	font-family: Arial, Helvetica, sans-serif;\n" + 
              		"	font-size: 50px;\n" + 
              		"}\n" + 
              		"\n" + 
              		".active {\n" + 
              		"    background-color: #4CAF50;\n" + 
              		"}\n" + 
              		"</style>\n" + 
              		"<script type=\"text/javascript\" src=\"ConexionPrediciones.js\"></script>\n" + 
              		"</head>\n" + 
              		"<body>\n" + 
              		"\n" + 
              		"<nav class=\"navbar navbar-default\">\n" + 
              		"        <div class=\"navbar-header\">\n" + 
              		"            <a class=\"navbar-brand\" href=\"#\">\n" + 
              		"           </a>\n" + 
              		"            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\n" + 
              		"              <span class=\"sr-only\">Toggle navigation</span>\n" + 
              		"              <span class=\"icon-bar\"></span>\n" + 
              		"              <span class=\"icon-bar\"></span>\n" + 
              		"              <span class=\"icon-bar\"></span>\n" + 
              		"            </button>\n" + 
              		"        </div>\n" + 
              		"        <div class=\"container\">\n" + 
              		"            <div class=\"col-sm-6 col-md-9\">\n" + 
              		"                <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n" + 
              		"                    <ul id=\"botonera\">\n" + 
              		"                        \n" + 
              		"                        \n" + 
              		"                        <div class=\"oculto\">\n" + 
              		"                            \n" + 
              		"                        </div>\n" + 
              		"                        <li class=\"width-auto\"> <a href=\"inicioAdmin.html\">Inicio</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto \"><a href=\"Aceites.html\">Aceites Cocina</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto \"><a href=\"AceitesOliva.html\">Aceites Olivas</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto \"><a href=\"Margarina.html\">Margarinas</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto \"><a href=\"Vinagres.html\">Vinagres</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto \"><a href=\"Bebidas.html\">Aceites</a></li>\n" + 
              		"                        <li class=\"width-auto line\">|</li>\n" + 
              		"                        <li class=\"width-auto\"><a href=\"/page/contacto\">Contacto</a></li>\n" + 
              		"                        <div class=\"col-xs-10 col-md-10 sin-padding-left\">\n" + 
              		"\n" + 
              		"                        </form>\n" + 
              		"                        </div>\n" + 
              		"                    </ul>\n" + 
              		"                </div>\n" + 
              		"            </div>\n" + 
              		"        </div>\n" + 
              		"    </nav>\n" + 
              		"<div>\n" + 
              		"    <h1 >\n" + 
              		"    Aceites\n" + 
              		"    </h1>\n" + 
              		"    <div>\n" + 
              		"    <table style=\"width:80%\" align=\"center\">\n" + 
              		"\n" + 
              		"    </table>\n" + 
              		"    <div class=\"container\">\n" + 
              		"        <canvas id=\"myChart\"></canvas>\n" + 
              		"        \n" + 
              		"    </div>\n" + 
              		"    <script>\n" + 
              		"        var myChart = document.getElementById('myChart').getContext('2d');\n" + 
              		"\n" + 
              		"        var massPopChart = new Chart(myChart, {\n" + 
              		"            type: 'line',\n" + 
              		"            data: {\n" + 
              		"                labels:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],\n" + 
              		"                datasets:[{\n" + 
              		"                    label: 'Operarios',\n" + 
              		"                    data: [\n" + 
              		"                        10,\n" + 
              		"                        11,\n" + 
              		"                        12,\n" + 
              		"                        13,\n" + 
              		"                        14,\n" + 
              		"                        15,\n" + 
              		"                        16,\n" + 
              		"                        17,\n" + 
              		"                        18,\n" + 
              		"                        19,\n" + 
              		"                        20,\n" + 
              		"                        21\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'lightGreen',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                }, \n" + 
              		"                {\n" + 
              		"                    label: 'Administrativos',\n" + 
              		"                    data: [\n" + 
              		"                        22,\n" + 
              		"                        23,\n" + 
              		"                        24,\n" + 
              		"                        25,\n" + 
              		"                        26,\n" + 
              		"                        27,\n" + 
              		"                        28,\n" + 
              		"                        29,\n" + 
              		"                        30,\n" + 
              		"                        31,\n" + 
              		"                        32,\n" + 
              		"                        33\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'red',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                },\n" + 
              		"                {\n" + 
              		"                    label: 'Ejecutivos',\n" + 
              		"                    data: [\n" + 
              		"                        34,\n" + 
              		"                        35,\n" + 
              		"                        36,\n" + 
              		"                        37,\n" + 
              		"                        38,\n" + 
              		"                        39,\n" + 
              		"                        40,\n" + 
              		"                        41,\n" + 
              		"                        42,\n" + 
              		"                        43,\n" + 
              		"                        44,\n" + 
              		"                        45\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'yellow',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                },\n" + 
              		"                {\n" + 
              		"                    label: 'Altos Ejecutivos',\n" + 
              		"                    data: [\n" + 
              		"                        46,\n" + 
              		"                        47,\n" + 
              		"                        48,\n" + 
              		"                        49,\n" + 
              		"                        50,\n" + 
              		"                        51,\n" + 
              		"                        52,\n" + 
              		"                        53,\n" + 
              		"                        54,\n" + 
              		"                        55,\n" + 
              		"                        56,\n" + 
              		"                        57\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'Gray',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                },\n" + 
              		"                {\n" + 
              		"                    label: '18-30',\n" + 
              		"                    data: [\n" + 
              		"                        10,\n" + 
              		"                        15,\n" + 
              		"                        10,\n" + 
              		"                        15,\n" + 
              		"                        11,\n" + 
              		"                        16,\n" + 
              		"                        12,\n" + 
              		"                        17,\n" + 
              		"                        13,\n" + 
              		"                        18,\n" + 
              		"                        14,\n" + 
              		"                        21\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'pink',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                },\n" + 
              		"                {\n" + 
              		"                    label: '31-50',\n" + 
              		"                    data: [\n" + 
              		"                        20,\n" + 
              		"                        23,\n" + 
              		"                        21,\n" + 
              		"                        24,\n" + 
              		"                        22,\n" + 
              		"                        25,\n" + 
              		"                        23,\n" + 
              		"                        26,\n" + 
              		"                        24,\n" + 
              		"                        27,\n" + 
              		"                        28,\n" + 
              		"                        29\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'blue',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                },\n" + 
              		"                                {\n" + 
              		"                    label: '50<',\n" + 
              		"                    data: [\n" + 
              		"                        20,\n" + 
              		"                        20,\n" + 
              		"                        20,\n" + 
              		"                        23,\n" + 
              		"                        23,\n" + 
              		"                        23,\n" + 
              		"                        23,\n" + 
              		"                        23,\n" + 
              		"                        25,\n" + 
              		"                        25,\n" + 
              		"                        25,\n" + 
              		"                        25\n" + 
              		"                    ],\n" + 
              		"                    backgroundColor:'brown',\n" + 
              		"                    borderWidth:1,\n" + 
              		"                    borderColor: 'black',\n" + 
              		"                    fill: false,\n" + 
              		"                }\n" + 
              		"                ]\n" + 
              		"            },\n" + 
              		"            options:{}\n" + 
              		"        });\n" + 
              		"    </script>\n" + 
              		"</div>\n" + 
              		"<table>\n" + 
              		"	<tr>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"1\" Value=\"1\">\n" + 
              		"			<label for=\"1\"><font color=\"white\"><h3>ESPACIO</h3> </font></label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"2\" Value=\"2\">\n" + 
              		"			<label for=\"2\"><font color=\"white\"><h3>ESPACIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"3\" Value=\"3\">\n" + 
              		"			<label for=\"3\"><font color=\"white\"><h3>ESPACIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"4\" Value=\"4\">\n" + 
              		"			<label for=\"4\"> <font color=\"white\"><h3>ESPACI0OO</h3> </font></label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"5\" Value=\"5\">\n" + 
              		"			<label for=\"5\"><font color=\"white\"><h3>ESPACIIOOO</h3> </font></label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"6\" Value=\"6\">\n" + 
              		"			<label for=\"6\"><font color=\"white\"><h3>ESPACIIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"7\" Value=\"7\">\n" + 
              		"			<label for=\"7\"> <font color=\"white\"><h3>ESPACIOOO</h3> </font></label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"8\" Value=\"8\">\n" + 
              		"			<label for=\"8\"><font color=\"white\"><h3>ESPACIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"9\" Value=\"9\">\n" + 
              		"			<label for=\"9\"><font color=\"white\"><h3>ESPACOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"10\" Value=\"10\">\n" + 
              		"			<label for=\"10\"><font color=\"white\"><h3>ESPACIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"11\" Value=\"11\">\n" + 
              		"			<label for=\"11\"> <font color=\"white\"><h3>ESPACIIOOO</h3> </font></label>\n" + 
              		"		</th>\n" + 
              		"		<th>\n" + 
              		"			<input type=\"radio\" name=\"mes\" id=\"12\" Value=\"12\">\n" + 
              		"			<label for=\"12\"><font color=\"white\"><h3>ESPACIIOOO</h3> </font> </label>\n" + 
              		"		</th>\n" + 
              		"	</tr>\n" + 
              		"</table>\n" + 
              		"       \n" + 
              		"       \n" + 
              		"\n" + 
              		"</div>\n" + 
              		"<div>\n" + 
              		"\n" + 
              		"\n" + 
              		"	<h1> Sugerencias</h1>\n" + 
              		"<section class=\"Bogota\">\n" + 
              		"	<h3> Recomendaciones Bogota</h3>\n" + 
              		"	<input type=\"radio\" name=\"Sede\" id=\"a\" Value=\"Bogota1\">\n" + 
              		"</section>\n" + 
              		"<section class =\"Buga\">\n" + 
              		"	<h3> Recomendaciones Buga</h3>\n" + 
              		"	<input type=\"radio\" name=\"Sede\" Value=\"Buga\" id=\"b\">	\n" + 
              		"</section>\n" + 
              		"<section class= \"Barranquilla\">\n" + 
              		"	<h3> Recomendaciones Barranquilla</h3>\n" + 
              		"	<input type=\"radio\" name=\"Sede\" Value=\"Barranquilla\" id=\"c\">		\n" + 
              		"</section>\n" + 
              		"\n" + 
              		"<form action=\"AceiteOlivetto\" method=\"get\" name=\"formulario1\">\n" + 
              		"<input type=\"text\" name=\"Sede\" id=\"SedeFormulario\" required style=\"visibility:hidden\"> \n" + 
              		"<input type=\"number\" name=\"Mes\" id=\"MesFormulario\" required style=\"visibility:hidden\">\n" + 
              		"<input type=\"button\" value=\"Ver\" onclick=\"Envia()\">\n" + 
              		"</form> \n" + 
              		"<br>\n" + "<p>El estimado de productos necesarios con el que deberia abastecer el almacen ubicado en "
      				+ ""+Sede+" para el mes de " +( Mes == 1 ? "Enero":(Mes == 2) ? "Febrero":(Mes==3) ? "Marzo":(Mes==4)?"Abril":
      					(Mes==5)?"Mayo":(Mes==6)?"Junio":(Mes==7)?"Julio":(Mes==8)?"Agosto":(Mes==9)?"Septiembre":(Mes==10)?
      							"Octubre":(Mes==11)?"Noviembre":"Diciembre") + " corresponde a " + (int)(Float.parseFloat(res)*350) + 
      				".</p>"+
              		"<br>\n" + 
              		"<br>\n" + 
              		"<br>\n" + 
              		"<br>\n" + 
              		"<style>\n" + 
              		".footer {\n" + 
              		"    position: fixed;\n" + 
              		"    left: 0;\n" + 
              		"    bottom: 0;\n" + 
              		"    width: 100%;\n" + 
              		"    background-color: #777777;\n" + 
              		"    color: white;\n" + 
              		"    text-align: center;\n" + 
              		"}\n" + 
              		"</style>\n" + 
              		"\n" + 
              		"<div class=\"footer\">\n" + 
              		"  <p><img src=\"imagen/logo-footer.png\" width=\"100\" height=\"50\"></p>\n" + 
              		"</div>\n" + 
              		"\n" + 
              		"\n" + 
              		"\n" + 
              		"</body></html>");
              
} catch (IOException e) {
	System.out.println("The URL is not valid.");
	System.out.println(e.getMessage());
}
finally {
	if (tokenConnection != null) {
		tokenConnection.disconnect();
	}
	if (tokenBuffer != null) {
		tokenBuffer.close();
	}
	if (scoringConnection != null) {
		scoringConnection.disconnect();
	}
	if (scoringBuffer != null) {
		scoringBuffer.close();
	}
}

  }
}