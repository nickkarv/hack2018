import java.io.IOException;
import java.io.*;
import java.net.MalformedURLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "MargarinaCampi",
    urlPatterns = {"/MargarinaCampi"}
)
public class MargarinaCampi extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
      
      int Mes = Integer.parseInt(request.getParameter("Mes"));
      String Sede = request.getParameter("Sede");      
     
      String  wml_service_credentials_url = "https://us-south.ml.cloud.ibm.com";
String wml_service_credentials_username = "fc8afda2-f092-4f9f-aeb2-7a9da0be7977";
      String wml_service_credentials_password = "4abcb306-1c95-40c3-ada1-9c30e54df3a6";
      Map<String, String> wml_credentials = new HashMap<String, String>()
	{{
		put("url", wml_service_credentials_url);
		put("username", wml_service_credentials_username);
		put("password", wml_service_credentials_password);
	}};

String wml_auth_header = "Basic " +
		Base64.getEncoder().encodeToString((wml_credentials.get("username") + ":" +
			wml_credentials.get("password")).getBytes(StandardCharsets.UTF_8));
      System.out.print(wml_credentials.get("username")+"\n" + wml_credentials.get("password")+"\n"+wml_credentials.get("url")+"\n" );
String wml_url = wml_credentials.get("url") + "/v3/identity/token";
HttpURLConnection tokenConnection = null;
HttpURLConnection scoringConnection = null;
BufferedReader tokenBuffer = null;
BufferedReader scoringBuffer = null;
try {
	// Getting WML token
	URL tokenUrl = new URL(wml_url);
              System.out.print(tokenUrl +"\n");
	tokenConnection = (HttpURLConnection) tokenUrl.openConnection();
              System.out.print(tokenConnection +"\n");
	tokenConnection.setDoInput(true);
	tokenConnection.setDoOutput(true);
	tokenConnection.setRequestMethod("GET");
	tokenConnection.setRequestProperty("Authorization", wml_auth_header);
              System.out.print("haed;;; " + wml_auth_header +"\n");
	tokenBuffer = new BufferedReader(new InputStreamReader(tokenConnection.getInputStream()));
	StringBuffer jsonString = new StringBuffer();
	String line;
	while ((line = tokenBuffer.readLine()) != null) {
		jsonString.append(line);
	}
              
	// Scoring request
	URL scoringUrl = new URL("https://us-south.ml.cloud.ibm.com/v3/wml_instances/3c03c53a-dc91-4c55-ae5a-506740304f3a/deployments/9e76da43-cc3e-4aaa-8e20-cd1d4688c539/online");
              System.out.print(scoringUrl +"\n");
	String wml_token = "Bearer " +
			jsonString.toString()
					.replace("\"","")
					.replace("}", "")
					.split(":")[1];
              System.out.print(wml_token +"\n");
	scoringConnection = (HttpURLConnection) scoringUrl.openConnection();
              System.out.print(scoringUrl +"\n");
	scoringConnection.setDoInput(true);
	scoringConnection.setDoOutput(true);
	scoringConnection.setRequestMethod("POST");
	scoringConnection.setRequestProperty("Accept", "application/json");
	scoringConnection.setRequestProperty("Authorization", wml_token);
	scoringConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	OutputStreamWriter writer = new OutputStreamWriter(scoringConnection.getOutputStream(), "UTF-8");

	// NOTE: manually define and pass the array(s) of values to be scored in the next line
	String payload = "{\"fields\": [\"Mes\", \"Sedes\"], \"values\": [["+Mes+", \""+Sede+"\"]]}";
	System.out.print(payload +"\n");
              writer.write(payload);                        
	writer.close();

	scoringBuffer = new BufferedReader(new InputStreamReader(scoringConnection.getInputStream()));
	StringBuffer jsonStringScoring = new StringBuffer();
	String lineScoring;
	while ((lineScoring = scoringBuffer.readLine()) != null) {
		jsonStringScoring.append(lineScoring);
	}
	System.out.println(jsonStringScoring);
              int cuentacomas = 0;
              String res = "";
              String res2 = "";
              boolean terminerecorrerprimero = false;
              for (int i = jsonStringScoring.length() -1; i > 0; i--) {
                  if (jsonStringScoring.charAt(i) == ',') {
                      cuentacomas++;
                  }
                  if (cuentacomas ==  4) {
                      for (int j = i - 1;;j--) {
                        if (jsonStringScoring.charAt(j-1) != ',' && !terminerecorrerprimero) {
                          res += jsonStringScoring.charAt(j-1);
                        }
                        else {
                            terminerecorrerprimero = true;
                            if (jsonStringScoring.charAt(j-2) == '[') {
                                break;
                            }
                            res2+= jsonStringScoring.charAt(j-2);
                        }
                      }
                      break;
                  }
              }
              String sCadena = res;
              String sCadenaInvertida="";
              for (int x=sCadena.length()-1;x>=0;x--){
                  sCadenaInvertida = sCadenaInvertida + sCadena.charAt(x);
              }
              res=sCadenaInvertida;
              sCadena = res2;
              sCadenaInvertida = "";
              for (int x=sCadena.length()-1;x>=0;x--){
                  sCadenaInvertida = sCadenaInvertida + sCadena.charAt(x);
              }
              res2=sCadenaInvertida;
              res = res.replaceAll(" ", "");
              float ProbExito=Float.parseFloat(res);
              float ProbFallo=Float.parseFloat(res2);
              System.out.println(res);
              System.out.println(res2);
              
              response.getWriter().print("<!DOCTYPE HTML> <html> <head> </head> <body> <h1> efectivamente la probabilidad es de "+res+"</h1> </body> </html>");
              
} catch (IOException e) {
	System.out.println("The URL is not valid.");
	System.out.println(e.getMessage());
}
finally {
	if (tokenConnection != null) {
		tokenConnection.disconnect();
	}
	if (tokenBuffer != null) {
		tokenBuffer.close();
	}
	if (scoringConnection != null) {
		scoringConnection.disconnect();
	}
	if (scoringBuffer != null) {
		scoringBuffer.close();
	}
}

  }
}